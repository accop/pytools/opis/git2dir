# -*- coding: utf-8 -*-

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # #                                                                                   # # # #
# # # #                                                                                   # # # #
# # # #                         - - - - - - - - - - - - - - - - - -                       # # # #
# # # #                                                                                   # # # #
# # # #                     ESS OPIs: Git Repositories to Directories                     # # # #
# # # #                                                                                   # # # #
# # # #                         - - - - - - - - - - - - - - - - - -                       # # # #
# # # #                                                                                   # # # #
# # # #   Author: Benjamin Bolling                                                        # # # #
# # # #   Maintainer: Benjamin Bolling (benjamin.bolling@ess.eu)                          # # # #
# # # #   Affiliation: European Spallation Source ERIC                                    # # # #
# # # #   Initialization date: 2020-10-01                                                 # # # #
# # # #   Milestone 1, 2020-10-01: Script is working, 10-ACC 999-RF and 999-PBI included. # # # #
# # # #                                                                                   # # # #
# # # #   Last updated: 2025-01-14.                                                       # # # #
# # # #                                                                                   # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # #                                                                                   # # # #
# # # #   Input/output functions for the generator script.                                # # # #
# # # #                                                                                   # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

import errno, os, stat, shutil, json
from subprocess import call, STDOUT

def clear_dir(path):
    shutil.rmtree(path, ignore_errors=False, onerror=handle_remove_readonly)

def handle_remove_readonly(func, path, exc):
  excvalue = exc[1]
  if func in (os.rmdir, os.remove, os.unlink) and excvalue.errno == errno.EACCES:
      os.chmod(path, stat.S_IRWXU| stat.S_IRWXG| stat.S_IRWXO) # 0777
      func(path)
  else:
      raise

def createDirWithRepos(thisdir,destination,repository):
    os.chdir(os.path.join(*destination))
    if call(["git", "branch"], stderr=STDOUT, stdout=open(os.devnull, 'w')) != 0:
        os.system("git init")
        os.system("git remote add origin "+repository)
    x = os.system("git pull origin master")
    if x != 0:
        x = os.system("git pull origin main")
        if x != 0:
            print("---------------------------")
            print("---------- ERROR ----------")
            print("Unable to fetch master/main from repository: {}".format(repository))
            print("---------- ERROR ----------")
            print("---------------------------")
    os.system("git submodule update --init --recursive")
    os.chdir(thisdir)

def setupDestination(thisdir,destination):
    currDest = thisdir
    for dest in destination:
        currDest = os.path.join(currDest,dest)
        if os.path.exists(currDest) == False:
            os.mkdir(currDest)

def run_generator(opis_only,nav_only):
    thisdir = os.path.dirname(os.path.realpath(__file__))
    with open('repos.json') as json_file:
        loadfile = json.load(json_file)
        metadata = loadfile[0]
        opidata = loadfile[1]
        otherdata = loadfile[2]

        # build opi files struct
        if not nav_only:
            for repo in opidata:
                dest = repo['dest'].split('/')
                dest.insert(0,'opis')
                giturl = repo['url']
                setupDestination(thisdir,dest)
                createDirWithRepos(thisdir,dest,giturl)

        # build navigators
        if not opis_only:
            dest = 'navigators'
            setupDestination(thisdir,dest.split('/'))
            createDirWithRepos(thisdir,dest.split('/'),otherdata[dest])
        
        # print(navdata)
        
        for md in metadata:
            print(' ')
            print(md)
        
