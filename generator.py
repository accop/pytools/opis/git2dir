# -*- coding: utf-8 -*-

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # #                                                                                   # # # #
# # # #                                                                                   # # # #
# # # #                         - - - - - - - - - - - - - - - - - -                       # # # #
# # # #                                                                                   # # # #
# # # #                     ESS OPIs: Git Repositories to Directories                     # # # #
# # # #                                                                                   # # # #
# # # #                         - - - - - - - - - - - - - - - - - -                       # # # #
# # # #                                                                                   # # # #
# # # #   Author: Benjamin Bolling                                                        # # # #
# # # #   Maintainer: Benjamin Bolling (benjamin.bolling@ess.eu)                          # # # #
# # # #   Affiliation: European Spallation Source ERIC                                    # # # #
# # # #   Initialization date: 2020-10-01                                                 # # # #
# # # #   Milestone 1, 2020-10-01: Script is working, 10-ACC 999-RF and 999-PBI included. # # # #
# # # #                                                                                   # # # #
# # # #   Last updated: 2025-01-14.                                                       # # # #
# # # #                                                                                   # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # #                                                                                   # # # #
# # # #                         - - - - - - - - - - - - - - - - - -                       # # # #
# # # #                                                                                   # # # #
# # # #                                   Requirements:                                   # # # #
# # # #                                                                                   # # # #
# # # #                         - - - - - - - - - - - - - - - - - -                       # # # #
# # # #                                                                                   # # # #
# # # #         - Python 3.x                                                              # # # #
# # # #         - Git                                                                     # # # #
# # # #                                                                                   # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # #                                                                                   # # # #
# # # #                         - - - - - - - - - - - - - - - - - -                       # # # #
# # # #                                                                                   # # # #
# # # #          Git repo:  https://gitlab.esss.lu.se/accop/pytools/opis/git2dir          # # # #
# # # #                                                                                   # # # #
# # # #                         - - - - - - - - - - - - - - - - - -                       # # # #
# # # #                                                                                   # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

import os, shutil, argparse
from sys import exit
import urllib.request

version = '2.0.0'

parser = argparse.ArgumentParser()
parser.add_argument('--update', action='store_true', help='Dowload git2dir files, update and then exit.')
parser.add_argument('--fetch_only', action='store_true', help="Does not update any structure, only retrieves the available changes in your local opis/ file structure's git repositories.")
parser.add_argument('--nav_only', action='store_true', help="Only fetch navigators.")
parser.add_argument('--opis_only', action='store_true', help="Only fetch OPIs.")
parser.add_argument('--reset_all', action='store_true', help='Full reset that removes the opis/ directory recursively and replaces it with a fresh new clone of all.')
parser.add_argument('--version', action='store_true', help='Shows the version of this script and then exits.')
args = parser.parse_args()

def retreiveFile(fn):
    urllib.request.urlretrieve('https://gitlab.esss.lu.se/accop/pytools/opis/git2dir/-/raw/master/{}?ref_type=heads'.format(fn), fn)

if args.version:
    print('git2dir version: {}'.format(version))
    exit()

if args.update:
    files = ['generator.py','IO.py']
    for file in files:
        retreiveFile(file)
    print('Update completed. Restart script.')
    exit()
else:
    try:
        import IO
    except:
        if input('IO module not found. Download? [y/N] >> ') == 'y':
            retreiveFile('IO.py')
            print('IO downloaded. Restart script.')
        exit()
    try:
        IO.clear_dir('.git')
    except:
        pass

    if not args.fetch_only:
        retreiveFile('repos.json')

    if args.reset_all and os.path.isdir('opis'):
        if input('WARNING: This will force remove all content in opis/ recursively. Proceed? [y/N] >> ') == 'y':
            shutil.rmtree('opis')
            IO.run_generator(args.opis_only,args.nav_only)
        else:
            print('Cancelling.')
    else:
        IO.run_generator(args.opis_only,args.nav_only)
